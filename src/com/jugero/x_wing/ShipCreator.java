package com.jugero.x_wing;

import android.os.Parcel;
import android.os.Parcelable;

public class ShipCreator implements Parcelable.Creator<Ship> {

	@Override
	public Ship createFromParcel(Parcel source) {
		// TODO Auto-generated method stub
		return new Ship(source);
	}

	@Override
	public Ship[] newArray(int size) {
		// TODO Auto-generated method stub
		return new Ship[size];
	}

}
