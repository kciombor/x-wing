package com.jugero.x_wing;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

public class DiceRoll extends Activity{
	ImageButton roll;
	ImageButton plus;
	ImageButton minus;
	int diceNo=3;
	List<Integer> possibleRolls = new ArrayList<Integer>();
	Context context=this;
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dice_roll);
        Intent intent=getIntent();
        final String color = intent.getStringExtra("rollColor");
        if(color.equals("red")){
        	possibleRolls.add(R.drawable.die_red_blank);
        	possibleRolls.add(R.drawable.die_red_blank);
        	possibleRolls.add(R.drawable.die_red_critical);
        	possibleRolls.add(R.drawable.die_red_focus);
        	possibleRolls.add(R.drawable.die_red_focus);
        	possibleRolls.add(R.drawable.die_red_hit);
        	possibleRolls.add(R.drawable.die_red_hit);
        	possibleRolls.add(R.drawable.die_red_hit);
        }
        else{
        	possibleRolls.add(R.drawable.die_green_blank);
        	possibleRolls.add(R.drawable.die_green_blank);
        	possibleRolls.add(R.drawable.die_green_focus);
        	possibleRolls.add(R.drawable.die_green_focus);
        	possibleRolls.add(R.drawable.die_green_focus);
        	possibleRolls.add(R.drawable.die_green_evade);
        	possibleRolls.add(R.drawable.die_green_evade);
        	possibleRolls.add(R.drawable.die_green_evade);
        }
        for(int i=1;i<=6;i++){
        	int resID = getResources().getIdentifier("die"+Integer.toString(i), "id",getPackageName());
        	final ImageButton dice = (ImageButton)findViewById(resID);
        	resID = getResources().getIdentifier("die_"+color+"_none","drawable",getPackageName());
        	dice.setImageResource(resID);
        	dice.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final Random rand = new Random();
					Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);
					shake.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							dice.setImageResource(possibleRolls.get(rand.nextInt(possibleRolls.size())));
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub
							
						}
					});
					dice.setImageResource(getResources().getIdentifier("die_"+color+"_none","drawable",getPackageName()));
					dice.startAnimation(shake);
				}
			});
        }
        plus =(ImageButton) findViewById(R.id.plus);
        plus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(diceNo<6){
					int resID = getResources().getIdentifier("die"+Integer.toString(++diceNo), "id",getPackageName());
		        	ImageButton dice = (ImageButton)findViewById(resID);
		        	dice.setVisibility(View.VISIBLE);
				}
			}
		});
        minus =(ImageButton) findViewById(R.id.minus);
        minus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(diceNo>1){
					int resID = getResources().getIdentifier("die"+Integer.toString(diceNo--), "id",getPackageName());
		        	ImageButton dice = (ImageButton)findViewById(resID);
		        	dice.setVisibility(View.INVISIBLE);
				}
			}
		});
        roll= (ImageButton) findViewById(R.id.roll);
        roll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				for(int i=1;i<=diceNo;i++){
					Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);
					int resID = getResources().getIdentifier("die"+Integer.toString(i), "id",getPackageName());
		        	final ImageButton dice = (ImageButton)findViewById(resID);
		        	final Random rand = new Random();
					shake.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							dice.setImageResource(possibleRolls.get(rand.nextInt(possibleRolls.size())));
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub
							
						}
					});
					dice.setImageResource(getResources().getIdentifier("die_"+color+"_none","drawable",getPackageName()));
					dice.startAnimation(shake);
				}
			}
		});
        
	}
}
