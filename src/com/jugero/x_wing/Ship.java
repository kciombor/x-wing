package com.jugero.x_wing;

import android.os.Parcel;
import android.os.Parcelable;

public class Ship implements Parcelable {
	public static final ShipCreator CREATOR = new ShipCreator();
	String fraction;
	String type;
	boolean enabled=false;
	
	
	public Ship(String fraction){
		this.fraction=fraction;
	}
	
	public Ship(Parcel source){
        /*
         * Reconstruct from the Parcel
         */
        fraction = source.readString();
        type = source.readString();
        enabled = source.readByte() != 0; 
  }
	
	public void setType(String name){
		type=name.substring(5);
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(fraction);
		dest.writeString(type);
		dest.writeByte((byte) (enabled ? 1 : 0)); 
	}
}
