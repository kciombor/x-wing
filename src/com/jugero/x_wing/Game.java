package com.jugero.x_wing;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;

class shipMove{
	String speed;
	String type;
	String difficulty;
	ImageButton move_speed;
	ImageButton move_type;
	boolean chosen;
	shipMove(ImageButton speed,ImageButton type){
		move_speed=speed;
		move_type = type;
		chosen = false;
	}
}

public class Game extends Activity{
	ArrayList<Ship> ships = new ArrayList<Ship>();
	ImageButton shipButton;
	ImageButton diceRed;
	ImageButton diceGreen;
	ImageButton dial;
    ArrayList<shipMove> shipsMovements = new ArrayList<shipMove>();
    Context context = this;
    boolean state; //0-programowanie, 1-odslanianie
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_view);
        state=false;
        Intent intent=getIntent();
        ships = intent.getParcelableArrayListExtra("ships");
        String fraction = ships.get(1).fraction;
        for(int i=0;i<7;i++){
        	String shipButtonName = "game_ship_slot"+Integer.toString(i+1);
        	int resID = getResources().getIdentifier(shipButtonName, "id",getPackageName());
        	shipButton = (ImageButton)findViewById(resID);
        	String shipName = "ship_"+ships.get(i).type;
        	int shipPic = getResources().getIdentifier(shipName, "drawable",getPackageName());
        	shipButton.setImageResource(shipPic);
        	
        	String tag = "tag"+Integer.toString(i+1);
        	int tagID = getResources().getIdentifier(tag, "id",getPackageName());
        	ImageView tagView = (ImageView)findViewById(tagID);
        	
        	int m_speedID=getResources().getIdentifier("m_speed"+Integer.toString(i+1), "id",getPackageName());
        	int m_typeID=getResources().getIdentifier("m_type"+Integer.toString(i+1), "id",getPackageName());
        	shipsMovements.add(new shipMove((ImageButton)findViewById(m_speedID),(ImageButton)findViewById(m_typeID)));
        	shipsMovements.get(i).move_speed.setImageResource(getResources().getIdentifier("dial_"+fraction, "drawable",getPackageName()));
        	shipsMovements.get(i).move_type.setVisibility(View.INVISIBLE);
        	shipsMovements.get(i).move_type.setTag(ships.get(i).type);
        	final int shipNo = i;
        	if(state==false){
	        	shipsMovements.get(i).move_type.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(state==false){
							// TODO Auto-generated method stub
							Intent intent=new Intent(context,ShipMovement.class);
							intent.putExtra("shipType",(String)v.getTag());
							intent.putExtra("shipNo",Integer.toString(shipNo));
							startActivityForResult(intent,1);
						}
						else{
							if(shipsMovements.get(shipNo).chosen){
								if(shipsMovements.get(shipNo).difficulty.equals(""))
									 shipsMovements.get(shipNo).move_type.setImageResource(getResources().getIdentifier("m_"+shipsMovements.get(shipNo).type, "drawable",getPackageName()));
								 else
									 shipsMovements.get(shipNo).move_type.setImageResource(getResources().getIdentifier("m_"+shipsMovements.get(shipNo).type+"_"+shipsMovements.get(shipNo).difficulty, "drawable",getPackageName()));
								shipsMovements.get(shipNo).move_type.setVisibility(View.VISIBLE);
							}
						}
					}
				});
        	}
        	shipsMovements.get(i).move_speed.setTag(ships.get(i).type);
        	if(state==false){
	        	shipsMovements.get(i).move_speed.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(state==false){
							// TODO Auto-generated method stub
							Intent intent=new Intent(context,ShipMovement.class);
							intent.putExtra("shipType",(String)v.getTag());
							intent.putExtra("shipNo",Integer.toString(shipNo));
							startActivityForResult(intent,1);
						}
						else{
							if(shipsMovements.get(shipNo).chosen){
								shipsMovements.get(shipNo).move_speed.setImageResource(getResources().getIdentifier("m_"+shipsMovements.get(shipNo).speed, "drawable",getPackageName()));
								shipsMovements.get(shipNo).move_type.setVisibility(View.VISIBLE);
							}
						}
					}
				});
        	}
       
        	if(ships.get(i).enabled==false){
        		shipButton.setVisibility(View.INVISIBLE);
        		tagView.setVisibility(View.INVISIBLE);
        		shipsMovements.get(i).move_speed.setVisibility(View.INVISIBLE);
        		shipsMovements.get(i).move_type.setVisibility(View.INVISIBLE);
        	}
        }
        diceRed=(ImageButton)findViewById(R.id.red);
        diceRed.setOnClickListener(new OnClickListener() {
        	Intent gameIntent = new Intent(getApplicationContext(),DiceRoll.class);
			@Override
			public void onClick(View v) {
				gameIntent.putExtra("rollColor","red");
				startActivity(gameIntent);
			}
		});
        diceGreen=(ImageButton)findViewById(R.id.green);
        diceGreen.setOnClickListener(new OnClickListener() {
        	Intent gameIntent = new Intent(getApplicationContext(),DiceRoll.class);
			@Override
			public void onClick(View v) {
				gameIntent.putExtra("rollColor","green");
				startActivity(gameIntent);
			}
		});
        dial=(ImageButton)findViewById(R.id.dial);
        final int dialID = getResources().getIdentifier("dial_"+fraction, "drawable",getPackageName());
        dial.setImageResource(dialID);
        dial.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				state=!state;
				if(state==true){
					for(int j=0;j<7;j++){
						if(ships.get(j).enabled==true){
							shipsMovements.get(j).move_speed.setImageResource(R.drawable.dial_blank);
							shipsMovements.get(j).move_type.setVisibility(View.INVISIBLE);
						}
					}
					dial.setImageResource(R.drawable.dial_blank);
				}
				else{
					for(int j=0;j<7;j++){
						shipsMovements.get(j).chosen=false;
						shipsMovements.get(j).move_speed.setImageResource(dialID);
						shipsMovements.get(j).move_type.setVisibility(View.INVISIBLE);
					}
					dial.setImageResource(dialID);
				}
			}
		});
        
    }
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (data == null) {return;}
	    int shipNo = Integer.parseInt(data.getStringExtra("shipNo"));
	    String speed = data.getStringExtra("speed");
	    String type = data.getStringExtra("type");
	    String difficulty = data.getStringExtra("difficulty");
	    shipsMovements.get(shipNo).speed=speed;
	    shipsMovements.get(shipNo).difficulty=difficulty;
	    shipsMovements.get(shipNo).type=type;
	    shipsMovements.get(shipNo).move_speed.setImageResource(getResources().getIdentifier("m_"+speed, "drawable",getPackageName()));
	    if(difficulty.equals(""))
	    	shipsMovements.get(shipNo).move_type.setImageResource(getResources().getIdentifier("m_"+type, "drawable",getPackageName()));
	    else
	    	shipsMovements.get(shipNo).move_type.setImageResource(getResources().getIdentifier("m_"+type+"_"+difficulty, "drawable",getPackageName()));
	    shipsMovements.get(shipNo).move_type.setVisibility(View.VISIBLE);
	    shipsMovements.get(shipNo).chosen=true;
	  }
}
