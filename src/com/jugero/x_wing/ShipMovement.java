package com.jugero.x_wing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.jugero.x_wing.ManouverDescription;

public class ShipMovement extends Activity{
	String shipType;
	ImageButton manouver;
	ImageView shipPic;
	String shipNo;
	int row;
	int col;
	int speed;
	String type;
	String difficulty; 
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ship_movement);
        Intent intent = getIntent();
        shipType=intent.getStringExtra("shipType");
        shipNo = intent.getStringExtra("shipNo");
        manouver = (ImageButton)findViewById(R.id.manouver);
        shipPic = (ImageView)findViewById(R.id.shipPic);
        
        String shipName = "ship_"+shipType;
    	int shipPicView = getResources().getIdentifier(shipName, "drawable",getPackageName());
    	shipPic.setImageResource(shipPicView);
    	
        String shipManouverName = "manouver_"+shipType;
    	int shipManouverPic = getResources().getIdentifier(shipManouverName, "drawable",getPackageName());
    	manouver.setImageResource(shipManouverPic);
        
    	manouver.setOnTouchListener(new OnTouchListener() {
    		
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int x = (int)event.getX();
			    int y = (int)event.getY();
			    switch (event.getAction()) {
			        case MotionEvent.ACTION_DOWN:
			        case MotionEvent.ACTION_MOVE:
			        case MotionEvent.ACTION_UP:
			        	if(validateXY(x,y)){
			        		Intent intent=new Intent();
			        		intent.putExtra("shipNo",shipNo);
			        		intent.putExtra("speed", Integer.toString(speed));
			        		intent.putExtra("type", type);
			        		intent.putExtra("difficulty",difficulty);
			        		setResult(RESULT_OK,intent);
			        		finish();
			        	}
			    }
			    return false;
			}
		});
	}

    private boolean validateXY(int x, int y) {
    	row=y/143;
    	x-=170;
    	if(x<0){
    		return false;
    	}
    	col=x/136;
    	decodeRow(row,col);
    	if(speed >= 0)
    		return true;
    	else
    		return false;
    }

	private void decodeRow(int row, int col) {
		// TODO Auto-generated method stub
		if(row < 0 || col< 0 || row >= ManouverDescription.MAX_TABLE_ROW || col >= ManouverDescription.MAX_TABLE_ROW)
	    {
	        speed = -1;
	        return;
	    }
		speed = 5 - row;
		switch(col)
	    {
	        case 0: type = "turn_left"; break;
	        case 1: type = "bank_left"; break;
	        case 2: type = "straight"; break;
	        case 3: type = "bank_right"; break;
	        case 4: type = "turn_right"; break;
	        case 5: type = "koiogran";  break;
	        default: type = "stop"; break;
	    }
	    if(speed == 0)
	        type = "stop";
	    int value;
	    int index = row*ManouverDescription.MAX_TABLE_COLUMN + col;
	    int moveTab[];
	    moveTab = ManouverDescription.getShipTable(shipType);
	    value = moveTab[index];
	    difficulty = "";
	    if(value == 2)
	        difficulty = "green";
	    if(value == 3)
	        difficulty = "red";
	    if(value < 1)
	        speed = -1;
	}

}
