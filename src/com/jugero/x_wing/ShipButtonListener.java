package com.jugero.x_wing;

import java.util.List;

import com.jugero.x_wing.Ship;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class ShipButtonListener implements OnClickListener{
	int shipno=0;
	Context context;
	List<String> shipImages;
	List<Ship> ships;
	int shipIndex;
	ImageButton shipButton;
	public ShipButtonListener(int shipIndex,List<String> shipImages,Context context,List<Ship> ships, ImageButton imageButton){
		this.shipImages=shipImages;
		this.shipIndex=shipIndex;
		this.context=context;
		this.ships=ships;
		this.shipButton=imageButton;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String shipName = shipImages.get(shipno);
		int resID = context.getResources().getIdentifier(shipName, "drawable",context.getPackageName());
		ships.get(shipIndex).setType(shipName);
		shipno =(shipno+1)%shipImages.size();
		if(shipno==0)
			ships.get(shipIndex).enabled=false;
		else
			ships.get(shipIndex).enabled=true;
		shipButton.setImageResource(resID);
	}

}
