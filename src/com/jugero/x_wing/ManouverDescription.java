package com.jugero.x_wing;

public class ManouverDescription {
	public static final int MAX_TABLE_ROW = 6;
	public static final int MAX_TABLE_COLUMN = 6;
	public static final int XWingTable[] = {
			0,0,0,0,0, 0,
			0,0,1,0,0, 3,
			1,1,1,1,1, 0,
			1,1,2,1,1, 0,
			0,2,2,2,0, 0,	0,0,0,0,0, 0
		};
	public static final int YWingTable[] = {
			0,0,0,0,0, 0,
			0,0,3,0,0, 3,
			3,1,1,1,3, 0,
			1,1,2,1,1, 0,
			0,1,2,1,0, 0,	0,0,0,0,0, 0
		};
	public static final int AWingTable[] = {
			0,0,2,0,0, 3,
			0,0,2,0,0, 0,
			1,1,2,1,1, 3,
			2,2,2,2,2, 0,
			1,0,0,0,1, 0,	0,0,0,0,0, 0
		};
	public static final int FalconTable[] = {
			0,0,0,0,0, 0,
			0,0,1,0,0, 3,
			0,1,1,1,0, 3,
			1,1,2,1,1, 0,
			1,2,2,2,1, 0,	0,0,0,0,0, 0
		};
	public static final int BWingTable[] = {
		0,0,0,0,0, 0,
		0,0,3,0,0, 0,
		0,3,1,3,0, 0,
		1,1,2,1,1, 3,
		3,2,2,2,3, 0,	0,0,0,0,0, 0
	};
	public static final int HwkTable[] = {
		0,0,0,0,0, 0,
		0,0,3,0,0, 0,
		0,3,1,3,0, 0,
		1,1,2,1,1, 0,
		0,2,2,2,0, 0,	0,0,0,0,0, 0
	};
	public static final int TieFighterTable[] = {
			0,0,1,0,0, 0,
			0,0,1,0,0, 3,
			1,1,2,1,1, 3,
			1,2,2,2,1, 0,
			1,0,0,0,1, 0,	0,0,0,0,0, 0
		};
	public static final int TieAdvancedTable[] = {
			0,0,1,0,0, 0,
			0,0,1,0,0, 3,
			1,1,2,1,1, 0,
			1,1,2,1,1, 0,
			0,2,0,2,0, 0,	0,0,0,0,0, 0
		};
	public static final int TieInterceptorTable[] = {
			0,0,1,0,0, 3,
			0,0,2,0,0, 0,
			1,1,2,1,1, 3,
			2,2,2,2,2, 0,
			1,0,0,0,1, 0,	0,0,0,0,0, 0
		};
	public static final int SlaveTable[] = {
		0,0,0,0,0, 0,
		0,0,1,0,0, 3,
		1,1,1,1,1, 3,
		1,1,2,1,1, 0,
		0,2,2,2,0, 0,	0,0,0,0,0, 0
	};
	public static final int TieBomberTable[] = {
		0,0,0,0,0, 3,
		0,0,1,0,0, 0,
		1,1,2,1,1, 0,
		3,2,2,2,3, 0,
		0,1,2,1,0, 0,	0,0,0,0,0, 0
	};
	public static final int LambdaTable[] = {
		0,0,0,0,0, 0,
		0,0,0,0,0, 0,
		0,3,1,3,0, 0,
		3,1,2,1,3, 0,
		0,2,2,2,0, 0,	0,0,3,0,0, 0
	};
	
	public static final int[] getShipTable(String shipType){
		if(shipType.equals("xwing")){
			return XWingTable;
		}
		if(shipType.equals("ywing")){
			return YWingTable;
		}
		if(shipType.equals("awing")){
			return AWingTable;
		}
		if(shipType.equals("falcon")){
			return FalconTable;
		}
		if(shipType.equals("bwing")){
			return BWingTable;
		}
		if(shipType.equals("hwk")){
			return HwkTable;
		}
		if(shipType.equals("tiefighter")){
			return TieFighterTable;
		}
		if(shipType.equals("tieadvanced")){
			return TieAdvancedTable;
		}
		if(shipType.equals("tieinterceptor")){
			return TieInterceptorTable;
		}
		if(shipType.equals("slave")){
			return SlaveTable;
		}
		if(shipType.equals("tiebomber")){
			return TieBomberTable;
		}
		if(shipType.equals("lambda")){
			return LambdaTable;
		}
		assert (true);
		return null;
	}
}
