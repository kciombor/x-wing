package com.jugero.x_wing;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class SelectShips extends Activity{
	List <String> shipImages = new ArrayList<String>();
	List<ImageButton> shipsButtons = new ArrayList<ImageButton>();
	ArrayList<Ship> ships = new ArrayList<Ship>();
	ImageButton next;
	final Context context = this;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_ships);
        Intent intent=getIntent();
        String fraction = intent.getStringExtra("Fraction");
        if(fraction.equals("rebel")){
        	shipImages.add("ship_xwing");
        	shipImages.add("ship_ywing");
        	shipImages.add("ship_awing");
        	shipImages.add("ship_bwing");
        	shipImages.add("ship_falcon");
        	shipImages.add("ship_hwk");
        	shipImages.add("ship_slot");
        }
        else{
        	shipImages.add("ship_tiefighter");
        	shipImages.add("ship_tieadvanced");
        	shipImages.add("ship_tieinterceptor");
        	shipImages.add("ship_tiebomber");
        	shipImages.add("ship_lambda");
        	shipImages.add("ship_slave");
        	shipImages.add("ship_slot");
        }
        for(int i=0;i<7;i++){
        	ships.add(new Ship(fraction));
        	
        	String shipButtonName = "ship_slot"+Integer.toString(i+1);
        	int resID = getResources().getIdentifier(shipButtonName, "id",getPackageName());
        	shipsButtons.add((ImageButton) findViewById(resID));
        	shipsButtons.get(i).setOnClickListener(new ShipButtonListener(i, shipImages, getApplicationContext(), ships, shipsButtons.get(i)));
        }
        next = (ImageButton)findViewById(R.id.ready);
        next.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context,Game.class);
				intent.putParcelableArrayListExtra("ships",ships);
				startActivity(intent);
			}
		});
    }
}
